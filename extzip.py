#!/usr/bin/python

import os
import zipfile
import zlib

path = os.path.dirname(os.path.realpath(__file__))
files = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]

exclusions = ('.gitignore', 'extzip.py')

# Remove unwanted files from the file-list
for exclusion in exclusions:
	if exclusion in files:
		files.remove(exclusion)

# Dictionary key is filename without extension
# Dictionary value is a list of extensions
files_dictionary = {}

discovered_files: int = 0

# Build the dictionary of files
for file in files:
	file_ext_split = file.rsplit('.', 1)
	file_name = file_ext_split[0]

	file_ext = file_ext_split[1]

	if not file_name in files_dictionary:
		# Instantiate list if necessary
		files_dictionary[file_name] = []

	if file_ext != 'zip':
		files_dictionary[file_name].append(file_ext)
		discovered_files += 1

print("Discovered " + str(discovered_files) + " files to zip")

files_to_zip: int = 0

for file_name in files_dictionary:
	files_to_zip += len(files_dictionary[file_name])

file_zip_progress = 1
for file_name in files_dictionary:
	# If zip-file doesn't exist already, it will be created
	zf = zipfile.ZipFile(file_name + '.zip', mode='w')

	# Loop through extensions and add all of the files to the archive
	for extension in files_dictionary[file_name]:
		zf.write(file_name + '.' + extension, compress_type = zipfile.ZIP_DEFLATED)
		print("Zipped file " + str(file_zip_progress) + " of " + str(files_to_zip))
		file_zip_progress += 1

	zf.close()
